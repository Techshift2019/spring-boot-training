FROM openjdk:8
VOLUME /tmp
COPY /build/libs/spring-boot-training-1.0.0.jar spring-boot-training.jar
EXPOSE 8080
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /spring-boot-training.jar" ]
#ENTRYPOINT ["java","-jar","/spring-boot-training.jar"]
